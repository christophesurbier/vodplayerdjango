from django.contrib import admin
from models import Catalog
from models import Media

class CatalogAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['position', 'name', 'backgroundColor', 'textColor','refParent']}),
    ]
    list_display = ('position', 'name', 'backgroundColor', 'textColor','refParent',)
    list_filter = ('refParent',)
    ordering = ('position', 'name', 'refParent',)
    search_fields = ('name',)

class MediaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['catalogId', 'mediaName', 'description', 'thumbnail','year','streamingType','streamingUrl']}),
    ]
    list_display = ('catalogId', 'mediaName', 'description', 'thumbnailPreview','year','streamingType','streamingUrl',)
    list_filter = ('catalogId', 'mediaName',)
    ordering = ('catalogId', 'mediaName',)
    search_fields = ('mediaName',)


admin.site.register(Catalog,CatalogAdmin)
admin.site.register(Media,MediaAdmin)