from __future__ import unicode_literals

from django.db import models
from mysite import settings

# Create your models here.
class Catalog(models.Model):
    position = models.IntegerField()
    name = models.CharField(max_length=100)
    backgroundColor = models.CharField(max_length=7)
    textColor = models.CharField(max_length=7)
    refParent = models.ForeignKey('Catalog',null=True,blank=True,default=None)
    createdAt = models.DateTimeField(auto_now=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
    class Meta:
        unique_together = (
            ('name', 'refParent')
            )   
             
    def __unicode__(self):
        return u'%s' % (self.name)

class Media(models.Model):
    catalogId = models.ForeignKey('Catalog')
    mediaName = models.CharField(max_length=100)
    description = models.CharField(max_length=2000)
    thumbnail = models.ImageField(upload_to='media')
    year = models.CharField(max_length=5,null=True, blank=True)
    streamingType_choice = (
        (0, "video"),
        (1, "audio"),
    )
    streamingType = models.IntegerField(choices=streamingType_choice)
    streamingUrl = models.CharField(max_length=200)
    createdAt = models.DateTimeField(auto_now=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def thumbnailPreview(self):
        return """<img src="%s" width="110" height="135"/>""" % (settings.MEDIA_URL+self.thumbnail.name)

    thumbnailPreview.allow_tags = True

    def __unicode__(self):
        return u'%s - %s' % (self.catalogId,self.mediaName)
