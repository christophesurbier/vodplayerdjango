from models import Catalog,Media
from tastypie import fields
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie.authorization import Authorization

class MultiPartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.Meta.get('CONTENT_TYPE', 'application/json')
        if format == 'application/x-www-form-urlencoded':
            return request.POST
        if format.startswith('multipart'):
            data = request.PUT.copy()
            data.update(request.FILES)
            return data
        return super(MultiPartResource, self).deserialize(request, data, format)


class CatalogResource(ModelResource):
    refParent = fields.ForeignKey('catalog.api.CatalogResource', 'refParent', full=False, null=True) 
    class Meta:
        queryset = Catalog.objects.all()
        resource_name = 'catalog'
        collection_name = 'catalog'
        authorization = Authorization()
        filtering = {
            'refParent': ['exact'],
        }

class MediaResource(ModelResource):
    catalogId = fields.ForeignKey(CatalogResource, 'catalogId')
    class Meta:
        queryset = Media.objects.all()
        resource_name = 'media'
        collection_name = 'media'
        authorization = Authorization()
        filtering = {
            'catalogId': ['exact'],
            'mediaName': ALL_WITH_RELATIONS,
        }

