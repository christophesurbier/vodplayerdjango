from django.conf.urls import patterns, include, url
from django.contrib import admin

from mysite.views import Index
from catalog.api import CatalogResource,MediaResource
from tastypie.api import Api
from mysite import settings
from django.conf.urls.static import static

v1_api = Api(api_name='v1')
v1_api.register(CatalogResource())
v1_api.register(MediaResource())

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', Index.as_view(), name='index'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
)



